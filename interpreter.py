# 1. Lexer: String -> Tokens
# 2. Parser: Tokens -> AST
# 3. Interpreter: AST -> String

import re
import sys
from itertools import groupby

# Tokenize input program and perform optimization by reducing -/+ operators on the fly
def lex(program):
    tokens = []
    operators = [] # stack for reducing -/+ chars for optimization/simplification

    # remove whitespaces
    program = re.sub(r"\s+", "", program)
    while program:
        char = program[0]
        match = re.match(r'^([a-z]+)', program)
        if match: token = ('VAR', match.group(0))
        match = re.match(r'^([0-9]+)', program)
        if match: token = ('NUM', match.group(0))

        if char == '=':         token = ('ASSGN', '=')
        if char == ';':         token = ('END', ';')
        if char == '(':         token = ('LPAR', '(')
        if char == ')':         token = ('RPAR', ')')
        if char == '*':         token = ('OP', char)
        if char in ['-', '+']:
            if operators: # if there is +/- on the stack then reduce
                if operators.pop() == char: operators.append('+')  # ++/-- => +
                else:                       operators.append('-') # -+/+- => -
            else:
                operators.append(char)
            program = program[1:]
            continue
        
        if not token:
            raise Exception('Unexpected token: {}'.format(program))

        # Append reduced operator if there is one
        if operators:
            reduced_operator = ('OP', operators.pop())
            tokens.append(reduced_operator)
        
        tokens.append(token)
        program = program[len(token[1]):]
        
    return tokens


class Program:
    def __init__(self, assignments):
        # Represent environment as a Hash: { var: value }
        self.env = {}
        self.assignments = assignments

    def eval(self):
        for assignment in self.assignments:
            assignment.eval(self.env) # pass reference of env to assignment

    # Eval and display environment in stdout
    def run(self):
        self.eval()
        for var, value in self.env.iteritems():
            print "%s = %d" % (var, value)
        
    def display(self):
        print("Program ->")
        for assignment in self.assignments:
            assignment.display()
        print

class Assignment:
    def __init__(self, var, expr):
        self.var = var
        self.expr = expr

    def eval(self, env):
        # Simply set new var and its value to environment 
        env[self.var[1]] = self.expr.eval(env)

    def display(self):
        print("\tAssignment ->")
        print("\t\tVar ->"), self.var[1]
        print("\t\tExpr ->"),
        self.expr.display()
        print

# Expression Abstract Syntax Tree node
class ExprNode:

    # Map operators to functions
    OPS = {
        '+': lambda a, b : a + b,
        '-': lambda a, b : a - b,
        '*': lambda a, b : a * b
    }

    OP_PRIORITY = { '+': 0, '-': 0, '*': 1 }

    def __init__(self, val, right=None, left=None):
        self.left = left
        self.right = right
        self.val = val

    def node_type(self):
        return self.val[0]

    def node_val(self):
        return self.val[1]

    def eval(self, env):
        # If Number node then return value as Integer
        if self.node_type() == 'NUM':
            return int(self.node_val())
         # If Variable node then return corresponding value from environment or throw exception
        if self.node_type() == 'VAR':
            if not self.node_val() in env:
                 raise Exception('Undefined variable: {}'.format(self.node_val()))

            return env[self.node_val()]
        
        op = self.val[1] # Actual operator of node
        return self.OPS[op](self.left.eval(env), self.right.eval(env))

    # Print inorder traversal
    def display(self):
        if self.left: self.left.display() 
        print(self.val[1]), 
        if self.right: self.right.display()

def parse(tokens):
    # Splits list of tokens into sublists by END token
    assignments_tokens = [list(group) for k, group in groupby(tokens, lambda x: x[0] == 'END') if not k]

    assignments = []
    for assignment in assignments_tokens:
        assignments.append(parse_assignment(assignment))

    return Program(assignments)

def parse_assignment(tokens):
    var = tokens.pop(0)
    if tokens.pop(0)[0] != 'ASSGN':  # assignment character
        raise Exception('Expected assignment operator: {}')

    expr = parse_expression(tokens)

    return Assignment(var, expr)

def parse_expression(tokens):
    nodes = []      # stack of nodes
    operators = []  # stack of operators
    while tokens:
        token = tokens.pop(0)
        if token[0] == 'OP':
            while   operators and \
                    operators[-1][0] != 'LPAR' and \
                    ExprNode.OP_PRIORITY[operators[-1][1]] >= ExprNode.OP_PRIORITY[token[1]]:
                node = ExprNode(operators.pop(), nodes.pop(), nodes.pop())
                nodes.append(node)
            operators.append(token)
        elif token[0] == 'LPAR':
            operators.append(token)
        elif token[0] == 'RPAR':
            while operators[-1][0] != 'LPAR':
                node = ExprNode(operators.pop(), nodes.pop(), nodes.pop())
                nodes.append(node)
            # remove '(' from stack
            operators.pop()
        else:
            nodes.append(ExprNode(token))

    while operators:
        node = ExprNode(operators.pop(), nodes.pop(), nodes.pop())
        nodes.append(node)

    return nodes.pop()


if __name__ == "__main__":
    program = open(sys.argv[1]).read()
    parse(lex(program)).run()


# EXAMPLES & TESTS
exit(0)

parse(lex("x = 1; y = 2; z = y - x;")).display()
parse(lex("x = 1; y = 2; z = y - x;")).run()
parse(lex("a = 1 + 2 - 3 * (4 - 6);")).run()
parse(lex("x = 1; y = 2; z = (x+y)*(x-y);")).run()